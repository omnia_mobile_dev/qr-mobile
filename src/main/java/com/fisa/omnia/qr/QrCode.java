package com.fisa.omnia.qr;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.UUID;

public class QrCode {
    private final String text;
    private final int width;
    private final int height;
    private BitMatrix bitMatrix;

    public QrCode(String text, int size){
        if(size < 100 ){
            throw new IllegalArgumentException("Size should be greater that 100");
        }
        this.text = text;
        this.width = size;
        this.height = size;
    }

    public BufferedImage bufferedImageOfQR() throws WriterException {
        this.generateQR();
        return this.toBufferedImage();
    }

    public void streamOfQR(OutputStream stream) throws WriterException, IOException {
        this.generateQR();
        BufferedImage image = this.toBufferedImage();
        if (!ImageIO.write(image, "png", stream)) {
            throw new IOException("Could not write an image of stream! ");
        }
    }

    public String pathOfQR() throws WriterException, IOException {
        this.generateQR();
        String dir = System.getProperty("java.io.tmpdir") + File.separator + "qr" + File.separator;
        File qrCode = new File(dir + UUID.randomUUID() + ".png");
        Files.createDirectories(Paths.get(dir));
        BufferedImage image = this.toBufferedImage();
        ImageIO.write(image, "png", qrCode);
        return qrCode.getPath();
    }

    public String base64OfQR() throws WriterException, IOException {
        this.generateQR();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        streamOfQR(stream);
        return new String(Base64.getEncoder().encode(stream.toByteArray()));
    }




    private void generateQR() throws WriterException {
        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        this.bitMatrix = qrCodeWriter.encode(text, BarcodeFormat.QR_CODE, width, height);
    }

    private BufferedImage toBufferedImage() {
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB); // create an empty image
        int white = 255 << 16 | 255 << 8 | 255;
        int black = 0;
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                image.setRGB(i, j, bitMatrix.get(i, j) ? black : white);
            }
        }
        return image;
    }
}
