package com.fisa.omnia.qr.impl;

import com.fisa.omnia.mobile.QRService;
import com.fisa.omnia.qr.QrCode;
import com.google.zxing.WriterException;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;

public class QrService implements QRService {

    @Override
    public String pathOfQR(String text, int size) throws IOException, WriterException {
        QrCode qr = new QrCode(text,size);
        return qr.pathOfQR();
    }

    @Override
    public String base64OfQR(String text, int size) throws IOException, WriterException {
        QrCode qr = new QrCode(text,size);
        return qr.base64OfQR();
    }

    @Override
    public void streamOfQR(String text, int size, OutputStream stream) throws IOException, WriterException {
        QrCode qr = new QrCode(text,size);
        qr.streamOfQR(stream);
    }

    @Override
    public BufferedImage bufferedImageOfQR(String text, int size) throws WriterException {
        QrCode qr = new QrCode(text,size);
        return qr.bufferedImageOfQR();
    }
}
