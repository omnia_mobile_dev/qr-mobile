package com.fisa.omnia.qr.impl;

import com.fisa.omnia.mobile.QRService;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

public class Activator implements BundleActivator {

    private ServiceRegistration serviceReference = null;

    @Override
    public void start(BundleContext bundleContext) throws Exception {
        serviceReference = bundleContext.registerService(QRService.class, new QrService(), null);
    }

    @Override
    public void stop(BundleContext bundleContext) throws Exception {
        System.out.println("Stopping Omnia QR bundle");
        if (serviceReference != null) {
            serviceReference.unregister();
            serviceReference = null;
        }
    }
}
