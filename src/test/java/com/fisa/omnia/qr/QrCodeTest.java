package com.fisa.omnia.qr;

import static org.junit.Assert.assertTrue;

import com.fisa.omnia.qr.impl.QrService;
import com.google.zxing.WriterException;
import org.junit.Test;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

public class QrCodeTest
{
    private static final String DATA = "http://is_sunday_and_i_work.omnia/need_vacations";

    @Test
    public void pathTest() throws IOException, WriterException {
        QrService service = new QrService();
        File image = new File(service.pathOfQR(DATA,400));
        System.out.println(image.getPath());
        assertTrue( image.exists() );
    }

    @Test
    public void base64Test() throws IOException, WriterException {
        QrService service = new QrService();
        String base64Code = service.base64OfQR(DATA,400);
        System.out.println(base64Code);
        assertTrue( base64Code != null && base64Code.length() > 0 );
    }

    @Test
    public void bufferedImageTest() throws WriterException {
        QrService service = new QrService();
        BufferedImage image = service.bufferedImageOfQR(DATA,400);
        assertTrue(image != null && image.getWidth() == 400);
    }

    @Test
    public void streamTest() throws WriterException, IOException {
        QrService service = new QrService();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        service.streamOfQR(DATA,400,stream);
        assertTrue(stream.toByteArray().length > 0);
    }
}
